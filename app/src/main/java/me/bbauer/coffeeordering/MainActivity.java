package me.bbauer.coffeeordering;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int orderQuantity = 0;
    public boolean isWhippedCreamChecked = false;
    public boolean isChocolateChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void submitOrder(View view) {
        double price = this.calculatePrice();

        this.sendMail("default@recipient.com", price);
    }

    public void decreaseOrderQuantity(View view) {
        this.orderQuantity--;
        if (this.orderQuantity < 0) {
            this.orderQuantity = 0;
        }
        this.updateOrderQuantity();
    }

    public void increaseOrderQuantity(View view) {
        this.orderQuantity++;
        this.updateOrderQuantity();
    }

    public void whippedCreamChecked(View view) {
        CheckBox c = findViewById(R.id.includeWhippedCream);
        this.isWhippedCreamChecked = c.isChecked();
    }

    public void chocolateChecked(View view) {
        CheckBox c = findViewById(R.id.includeChocolate);
        this.isChocolateChecked = c.isChecked();
    }

    private void updateOrderQuantity() {
        TextView counterView = findViewById(R.id.quantity_text_view);
        counterView.setText(Integer.toString(this.orderQuantity));
    }

    private double calculatePrice() {
        double basePrice = this.orderQuantity * 5.0;
        double whippedCreamPrice = this.isWhippedCreamChecked ? this.orderQuantity * 1.0 : 0.0;
        double chocolatePrice = this.isChocolateChecked ? this.orderQuantity * 1.0 : 0.0;

        return basePrice + whippedCreamPrice + chocolatePrice;
    }

    private void sendMail(String receipient, double price) {
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Here's your coffee!");
        intent.putExtra(Intent.EXTRA_TEXT, "Your total is " + price + ". Please pay your order over at paypal.me/bbauer96");
        intent.setData(Uri.parse("mailto:" + receipient));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
